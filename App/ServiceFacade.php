<?php

namespace App;

class ServiceFacade
{
    protected $service;
    public $headers;

    /**
     * @param Service $service - сервис для передачи
     * @param array $headers - массив заголовков
     */
    public function __construct(Service $service, array $headers = array())
    {
        $this->service = $service;
    }

    /**
     * @param string $data - данные форм
     * @param array $headers - массив заголовков
     * @return mixed
     */
    public function get(string $data = "", array $headers = array())
    {
        $this->service->DTO = new DTO(new ServiceDTO(), $headers, $data, "GET");
        return $this->service->get();
    }

    /**
     * @param string $data
     * @param array $headers
     * @return mixed
     */
    public function post(string $data = "", array $headers = array())
    {
        $this->service->DTO = new DTO(new ServiceDTO(), $headers, $data, "POST");
        return $this->service->post();
    }

    /**
     * @param string $method - метод передачи сообщения
     * @param string $data
     * @param array $headers
     * @return mixed
     */
    public function put(string $method, string $data = "", array $headers = array())
    {
        $this->service->DTO = new DTO(new ServiceDTO(), $headers, $data, $method);
        return $this->service->post($method);
    }

    /**
     * @param string $data
     * @param array $headers
     * @return mixed
     */
    public function delete(string $data = "", array $headers = array())
    {
        $this->service->DTO = new DTO(new ServiceDTO(), $headers, $data, "DELETE");
        return $this->service->delete();
    }

    /**
     * @param string $method
     * @param string $data
     * @param array $headers
     * @return mixed
     */
    public function patch(string $method, string $data = "", array $headers = array())
    {
        $this->service->DTO = new DTO(new ServiceDTO(), $headers, $data, $method);
        return $this->service->post($method);
    }
}
