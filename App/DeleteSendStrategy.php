<?php

namespace App;

class DeleteSendStrategy implements SendStrategy
{
    private $headers;
    private $data;

    /**
     * @param DTO $DTO - объект с параметрами
     */
    public function __construct(DTO $DTO)
    {
        $this->headers = $DTO->headers;
        $this->data = $DTO->query;
    }
    public function send()
    {
        $hb = new HttpClientBuilder('https://httpbin.org/delete');
        $hc = $hb->addPOSTFIELDS($this->data)->addHTTPHEADER($this->headers)->send('delete')->build();
        return $hc->getOut();
    }
}
