<?php

namespace App;

class PostSendStrategy implements SendStrategy
{
    private $headers;
    private $data;
    private $method;

    /**
     * @param DTO $DTO - обънект с параметрами
     */
    public function __construct(DTO $DTO)
    {
        $this->headers = $DTO->headers;
        $this->data = $DTO->query;
        $this->method = $DTO->method;
    }

    public function send()
    {
        $hb = new HttpClientBuilder('https://httpbin.org/' . strtolower($this->method));
        //собираем сообщение
        $hc = $hb->addPOSTFIELDS($this->data)->addHTTPHEADER($this->headers)->send(strtolower($this->method))->build();
        return $hc->getOut();
    }
}
