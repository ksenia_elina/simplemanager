<?php

namespace App;

class DTO
{
    private $entity;

    /**
     * @param $model - класс с параметрами
     * @param $headers - заголовки сообщения
     * @param $query - данные сообщения
     * @param $method - метод отправки
     */
    public function __construct($model, $headers, $query, $method)
    {
        $model->headers = $headers;
        $model->query = $query;
        $model->method = $method;
        $this->entity = array($model);
        return $this;
    }

    /**
     * @param $name - название параметра
     * @return mixed
     */
    public function __get($name)
    {
        return $this->entity[0]->{$name};
    }
}
