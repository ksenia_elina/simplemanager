<?php

namespace App;

class HttpClient
{
    protected $out;

    /**
     * @param HttpClientBuilder $builder - передаем наш билдер
     */
    public function __construct(HttpClientBuilder $builder)
    {
        $this->out = $builder->out;
    }

    /**
     * @return mixed
     */
    public function getOut()
    {
        return $this->out;
    }
}
