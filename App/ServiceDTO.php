<?php

namespace App;

class ServiceDTO
{
    public $headers = array();
    public $query = array();
    public $method = "";
}
