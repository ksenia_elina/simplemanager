<?php

namespace App;

class GetSendStrategy implements SendStrategy
{
    private $headers;
    private $data;

    /**
     * @param DTO $DTO - объект с параметрами
     */
    public function __construct(DTO $DTO)
    {
        $this->headers = $DTO->headers;
        $this->data = $DTO->query;
    }

    public function send()
    {
        $hb = new HttpClientBuilder('https://httpbin.org/get' . $this->data);
        $hc = $hb->addHTTPHEADER($this->headers)->send('get')->build();
        return $hc->getOut();
    }
}
