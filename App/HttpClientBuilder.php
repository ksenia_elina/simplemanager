<?php

namespace App;

use mysql_xdevapi\Exception;
use pdeans\Http\Client;

class HttpClientBuilder
{
    private $ch;
    private $headers;
    private $data;
    private $url;
    public $out;

    /**
     * @param string $url - урл для отправки
     */
    public function __construct(string $url)
    {
        $client = new Client([
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_SSL_VERIFYHOST => 0,
        ]);
        $this->url = $url;
        $this->headers = array();
        $this->data = array();
        $this->ch = $client;
    }

    /**
     * @param string $postData - данные для отправки
     * @return $this
     */
    public function addPOSTFIELDS(string $postData = ""): HttpClientBuilder
    {
        $this->data = $postData;
        return $this;
    }

    /**
     * @param array $httpheader - заголовки сообщения
     * @return $this
     */
    public function addHTTPHEADER(array $httpheader = array()): HttpClientBuilder
    {
        $this->headers = $httpheader;
        return $this;
    }

    /**
     * @param string $method - метод отправки
     * @return $this
     */
    public function send(string $method): HttpClientBuilder
    {
        //тут происходит обработка нехорошего статуса
        try {
            $this->out = $this->ch->{$method}($this->url, $this->headers, $this->data);
            if ($this->out->getStatusCode() <> 200) {
                throw new \Exception($this->out->getReasonPhrase());
            }
        } catch (\Exception $e) {
            echo $e;
        }
        return $this;
    }

    public function build(): HttpClient
    {
        return new HttpClient($this);
    }
}
