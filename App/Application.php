<?php

namespace App;

final class Application
{
    private static $instance;

    private function __construct()
    {
    // Прячем конструктор
    }

    public static function getInstance(): Application
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private function __clone()
    {
        // Отключаем клонирование
        //
    }

    private function __wakeup()
    {
        // Отключаем десериализацию
    }

    public static function getSHSService(): ServiceFacade
    {
        return new ServiceFacade(new Service());
    }
}
