<?php

namespace App;

interface SendStrategy
{
    /**
     * @param DTO $DTO - передаем объект с параметрами
     */
    public function __construct(DTO $DTO);
    public function send();
}
