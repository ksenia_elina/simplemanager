<?php

namespace App;

class Service
{
    public $DTO;

    public function get()
    {
        $sender = new Sender(new GetSendStrategy($this->DTO));
        return $sender->send();
    }

    /**
     * @param string $method - метод передачи, по-умолчанию - пост
     * @return mixed
     */
    public function post(string $method = "POST")
    {
        $sender = new Sender(new PostSendStrategy($this->DTO));
        return $sender->send();
    }

    public function delete()
    {
        $sender = new Sender(new DeleteSendStrategy($this->DTO));
        return $sender->send();
    }
}
