<?php

namespace App;

class Sender
{
    protected $sender;

    /**
     * @param SendStrategy $sender - как будем отправлять сообщение
     */
    public function __construct(SendStrategy $sender)
    {
        $this->sender = $sender;
    }

    /**
     * @param string $data - данные для отправки
     * @param string $method - метод отправки
     * @return mixeds
     */
    public function send(string $data = "", string $method = "")
    {
        return $this->sender->send($data, $method);
    }
}
