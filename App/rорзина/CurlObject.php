<?
namespace App;

class CurlObject
{
protected $out;

public function __construct(CurlBuilder $builder)
{
$this->out = $builder->out;
}

    /**
     * @return mixed
     */
    public function getOut()
    {
        return $this->out;
    }
}