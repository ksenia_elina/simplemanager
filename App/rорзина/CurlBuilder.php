<?php

namespace App;

class CurlBuilder
{
    private $ch;

    public $out;

    public function __construct(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        $this->ch = $ch;
    }

    public function addCURLOPT_POST(): CurlBuilder
    {
        curl_setopt($this->ch, CURLOPT_POST, true);
        return $this;
    }
    public function addCURLOPT_CUSTOMREQUEST($req): CurlBuilder
    {
            curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, $req);
            return $this;
    }

    public function addCURLOPT_POSTFIELDS(string $postData = ""): CurlBuilder
    {
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $postData);
        return $this;
    }
    public function addCURLOPT_HTTPHEADER (array $httpheader = array()): CurlBuilder
    {
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $httpheader);
        return $this;
    }

    public function send(): CurlBuilder
    {
        $this->out = curl_exec($this->ch);

        /*$info = curl_getinfo($this->ch);
        echo "code: ${info['http_code']}";
        print_r($info['request_header']);

        $err = curl_error($this->ch);

        echo "error";
        var_dump($err);*/


        return $this;
    }

    public function build(): CurlObject
    {
    return new CurlObject($this);
    }
}
