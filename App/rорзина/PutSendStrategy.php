<?
namespace App;

class PutSendStrategy implements SendStrategy
{
    private $headers;
    public function __construct(array $headers = array())
    {
        $this->headers = $headers;
    }

    public function send(string $data = "")
    {
        $cb = new CurlBuilder('https://httpbin.org/put');
        $co = $cb->addCURLOPT_CUSTOMREQUEST('PUT')->addCURLOPT_POSTFIELDS($data)->addCURLOPT_HTTPHEADER($this->headers)->send()->build();
        return $co->getOut();
    }
}