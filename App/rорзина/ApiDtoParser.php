<?
namespace App;

class ApiDtoParser
{
    public static function fromDto($data):array
    {
        $returnArray=[];
        $decoded =json_decode($data,true);
        foreach($decoded as $one){
            $obj = new CurleResponseObject;
            foreach ($one as $key => $value) {
                $meth = "set". ucfirst(strtolower($key));
                $obj->{$key} = $value;
                break;
            }
            $returnArray[]=$obj;
            break;
        }
        return $returnArray;
    }
}