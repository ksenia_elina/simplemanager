<?
namespace App;
require_once 'vendor/autoload.php';
use pdeans\Http\Client;

use Psr\Http\Message\ResponseInterface;
// Define an error handler
function exception_error_handler1($errno, $errstr, $errfile, $errline) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
}
// функция обработки ошибок
function myErrorHandler($errno, $errstr, $errfile, $errline)
{
    if (!(error_reporting() & $errno)) {
        // Этот код ошибки не включён в error_reporting,
        // так что пусть обрабатываются стандартным обработчиком ошибок PHP
        return false;
    }

    // может потребоваться экранирование $errstr:
    $errstr = htmlspecialchars($errstr);

    switch ($errno) {
        case E_USER_ERROR:
            echo "<b>Пользовательская ОШИБКА</b> [$errno] $errstr<br />\n";
            echo "  Фатальная ошибка в строке $errline файла $errfile";
            echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
            echo "Завершение работы...<br />\n";
            exit(1);

        case E_USER_WARNING:
            echo "<b>Пользовательское ПРЕДУПРЕЖДЕНИЕ</b> [$errno] $errstr<br />\n";
            break;

        case E_USER_NOTICE:
            echo "<b>Пользовательское УВЕДОМЛЕНИЕ</b> [$errno] $errstr<br />\n";
            break;

        default:
            echo "Неизвестная ошибка: [$errno] $errstr<br />\n";
            break;
    }

    /* Не запускаем внутренний обработчик ошибок PHP */
    return true;
}
// Set your error handler
$old_error_handler = set_error_handler( function ($errno, $errstr, $errfile, $errline) {
    throw new \ErrorException($errstr, $errno, 0, $errfile, $errline);
});

try
{
    throw new \ErrorException('123');
}
catch(\ErrorException $e)
{
    echo $e;
}

$app = Application::getInstance();
$shsService = $app::getSHSService();
/*$getResult = $shsService->get();
var_dump($getResult);
$getResult = $shsService->post();
var_dump($getResult);
$getResult = $shsService->put();
var_dump($getResult);
$getResult = $shsService->delete();
var_dump($getResult);
$getResult = $shsService->patch();
var_dump($getResult);*/

$data = "perem=1";
$getResult = $shsService->get('?'.$data);
if($getResult instanceof ResponseInterface){
    print_r( true);
}
var_dump($getResult->getStatusCode());
var_dump($getResult->getHeaders());
var_dump($getResult->getBody()->getContents());
echo '<br>';
$getResult = $shsService->post($data);
var_dump($getResult->getStatusCode());
var_dump($getResult->getBody()->getContents());
echo '<br>';
echo '<br>';

$getResult = $shsService->put("PUT",$data);
var_dump($getResult->getStatusCode());
var_dump($getResult->getBody()->getContents());
echo '<br>';
echo '<br>';


$getResult = $shsService->delete($data);
var_dump($getResult->getStatusCode());
var_dump($getResult->getBody()->getContents());
echo '<br>';
echo '<br>';

$getResult = $shsService->patch("PATCH",$data);
var_dump($getResult->getStatusCode());
var_dump($getResult->getBody()->getContents());
echo '<br>';
echo '<br>';


echo '<br>';
$heders = array(
        "Authorization"=> "Bearer 12321321",
        "Content-Type"=> "multipart/form-data",
        "Content-Length" =>  strlen(file_get_contents('1.JPG')),
        "API-Key" => " abcdefghi" //Optional if required
);
$fields = file_get_contents('1.JPG');
$getResult = $shsService->post($fields,$heders);
var_dump($getResult->getStatusCode());
var_dump($getResult->getBody()->getContents());
echo '<br>';
echo '<br>';


$client = new Client([
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_SSL_VERIFYHOST => 0,
]);
// GET request with header
$response = $client->get('https://httpbin.org/get?perem=1');
var_dump($response->getStatusCode());



